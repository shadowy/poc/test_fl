import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:test_fl/pages/components/image_list.dart';
import '../main.dart';

class Task2Page extends StatefulWidget {
  const Task2Page({Key? key}) : super(key: key);

  @override
  State<Task2Page> createState() => _Task2PageState();
}

class _Task2PageState extends State<Task2Page> {
  late CameraController controller;
  int count = 0;
  List<File> photos = [];

  @override
  void initState() {
    super.initState();
    controller = CameraController(cameras[0], ResolutionPreset.max);
    controller.initialize().then((_) {
      if (!mounted) {
        return;
      }
      setState(() {});
    });
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  makePhoto() async {
    final XFile file = await controller.takePicture();
    photos.add(File(file.path));
    setState(() {});
  }

  int getCount() {
    return photos.length;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Task 2"),
        ),
        body: SizedBox(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Stack(
            children: [
              Align(
                  alignment: Alignment.center,
                  child: CameraPreview(
                    controller,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        ImageList(
                            counter: getCount,
                            getImage: (pos) => photos[pos],
                        ),
                        IconButton(
                          icon: const Icon(Icons.camera),
                          onPressed: () => makePhoto(),
                        )
                      ],
                    ),
                  ))
            ],
          ),
        ));
  }
}
