import 'package:flutter/material.dart';

import 'task-1.page.dart';
import 'task-2.page.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  navigateToTask1() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => const Task1Page()));
  }

  navigateToTask2() async {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => const Task2Page()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Navigation"),
      ),
      body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(4.0),
              child: TextButton(
                child: const Text("Task 1"),
                onPressed: () => navigateToTask1(),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(4.0),
              child: TextButton(
                child: const Text("Task 2"),
                onPressed: () => navigateToTask2(),
              ),
            ),
          ]),
    );
  }
}
