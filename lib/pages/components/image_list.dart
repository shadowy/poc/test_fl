import 'dart:io';

import 'package:flutter/material.dart';

typedef ImageListCounter = int Function();
typedef ImageListGet = File Function(int pos);

class ImageList extends StatefulWidget {
  const ImageList({Key? key, required this.counter, required this.getImage}) : super(key: key);

  final ImageListCounter counter;
  final ImageListGet getImage;

  @override
  State<ImageList> createState() => _ImageListState();
}

class _ImageListState extends State<ImageList> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: MediaQuery.of(context).size.width,
        height: 100,
        child: ListView.separated(
          padding: const EdgeInsets.all(8),
          scrollDirection: Axis.horizontal,
          itemCount: widget.counter(),
          itemBuilder: (BuildContext context, int index) => Image.file(widget.getImage(index)),
          separatorBuilder: (BuildContext context, int index) => Container(width: 10),
        ));
  }
}
